import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Space} from '../models/space.model';
import {TurnService} from '../services/turn.service';
import {ShipService} from "../services/ship.service";
import {Ship} from "../models/ship.model";

@Component({
  selector: 'app-board-space',
  templateUrl: './board-space.component.html',
  styleUrls: ['./board-space.component.css']
})
export class BoardSpaceComponent implements OnInit {
  @Input() space: Space;
  @Output() hitEvent: EventEmitter<any> = new EventEmitter<boolean>();
  @Output() shipSunk: EventEmitter<any> = new EventEmitter<Ship>();

  constructor(private turnService: TurnService, private shipService: ShipService) { }

  ngOnInit() {
  }

  fire() {
    //Cant fire on your own board or a space that has already been hit
    if (this.turnService.getTurn() === this.space.turnOrder || this.space.hit) {
      return;
    }

    this.space.hit = true;

    if (this.space.ship) {
      this.shipService.hit(this.space.ship);

      this.hitEvent.emit({
        hit: true,
        space: this.space
      });


      if (this.space.ship.isSunk) {
        this.shipSunk.emit({
          ship: this.space.ship,
          turnOrder: this.space.turnOrder
        });
      }
    } else {
      this.hitEvent.emit({
        hit: false,
        space: this.space
      });
    }

  }

  getButtonColor() {
    if (this.space.ship && this.space.hit) {
      return 'red';
    } else if (this.space.hit && this.space.shipAdj) {
      return 'green';
    } else if (this.space.hit) {
      return 'white';
    }
    if (this.space.ship && this.turnService.getTurn() === this.space.turnOrder) {
      return 'grey';
    }

  }

}
