import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule, Routes} from '@angular/router';
import { BoardSpaceComponent } from './board-space/board-space.component';
import {GameComponent} from './game/game.component';
import {TurnService} from "./services/turn.service";
import {ShipService} from "./services/ship.service";
import { LobbyComponent } from './lobby/lobby.component';
import {PlayerService} from "./services/player.service";


@NgModule({
  declarations: [
    AppComponent,
    BoardSpaceComponent,
    GameComponent,
    LobbyComponent
  ],
  imports: [
    RouterModule.forRoot([
      {
        path: 'game',
        component: GameComponent
      },
      {
        path: 'lobby',
        component: LobbyComponent
      },
      {
        path: '',
        redirectTo: '/lobby',
        pathMatch: 'full'
      }
    ]),
    BrowserModule
  ],
  providers: [
    TurnService,
    ShipService,
    PlayerService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
