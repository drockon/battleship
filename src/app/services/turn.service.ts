import {Player} from '../models/player.model';
import {Injectable} from '@angular/core';
/**
 * Created by dr483c on 8/6/2017.
 */
@Injectable()
export class TurnService {
  currentTurn: number;
  game: Player[];

  constructor() {
    if (sessionStorage.getItem('currentTurn') && sessionStorage.getItem('game')) {
      this.currentTurn = parseInt(sessionStorage.getItem('currentTurn'), 10);
      this.game = JSON.parse(sessionStorage.getItem('game'));
    } else {
      this.currentTurn = 0;
    }
  }

  setGame(game: Player[]) {
    this.game = game;
  }

  getTurn() {
    return this.currentTurn;
  }

  turnComplete() {
    this.currentTurn += 1;
    if (this.currentTurn > this.game.length - 1) {
      this.currentTurn = 0;
    }

    sessionStorage.setItem('currentTurn', this.currentTurn + '');
    //sessionStorage.setItem('game', JSON.stringify(this.game));
  }

  reset() {
    sessionStorage.setItem('currentTurn', '0');
    this.currentTurn = 0;
  }
}
