
import {Player} from '../models/player.model';
import {Space} from "../models/space.model";

export class PlayerService {

  pushSunkShips(player: Player) {
    //Decrement so removing element does not throw off index pos
    for (let i = player.ships.length - 1; i > -1 ; i--) {
      if (player.ships[i].isSunk){
        player.sunkShips.push(player.ships[i]);
        player.ships.splice(i, 1);
      }
    }
  }
  generateBoard(player: Player) {
    for (let y = 0; y < 8; y++){
      player.board[y] = new Array(8);
      for(let x = 0; x < 8; x++){
        player.board[y][x] = new Space(x, y);
        player.board[y][x].turnOrder = player.turnOrder;
      }
    }
  }

}
