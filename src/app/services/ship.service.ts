import {Injectable} from '@angular/core';
import {Space} from '../models/space.model';
import {Ship} from '../models/ship.model';
import {Orientation} from '../orientation.enum';
import {Point} from '../models/point.model';
/**
 * Created by dr483c on 8/6/2017.
 */
@Injectable()
export class ShipService {

  generatePosition(ship: Ship, board: Space[][]) {
    let validGen: boolean;
    do {
      validGen = true;

      //Random Gen Orientation and Origin
      ship.orientation = Math.floor(Math.random() * 3);
      ship.origin.y = Math.floor(Math.random() * (board.length - 1));
      ship.origin.x = Math.floor(Math.random() * (board[0].length - 1));

      //Check if position fits on the board
      let oppositeOriginPoint = this.transform(ship, ship.oppositeOrigin);
      if (!(board[oppositeOriginPoint.y] && board[oppositeOriginPoint.y][oppositeOriginPoint.x])) {
        //Didn't fit regenerate
        validGen = false;
        continue;
      }

      //Collision at origin
      if (board[ship.origin.y][ship.origin.x].ship){
        validGen = false;
        continue;
      }
      //Collision for rest of ship
      for (let i = 0; i < ship.offset.length; i++) {
        let currentOffset = this.transform(ship, ship.offset[i]);

        if (board[currentOffset.y][currentOffset.x].ship) {
          //Collision with another ship. Regen
          validGen = false;
          break; //Only breaks for loop not do/while
        }
      }
    }while (!validGen);
  }

  applyPosition(ship: Ship, board: Space[][]) {
    if (board[ship.origin.y][ship.origin.x].ship) {
      console.log(board[ship.origin.y][ship.origin.x]);
    }
    board[ship.origin.y][ship.origin.x].ship = ship;
    //transform offset and set space ship to ship ship
    for (let i = 0; i < ship.offset.length; i++) {
      let currentPoint = this.transform(ship, ship.offset[i]);
      board[currentPoint.y][currentPoint.x].ship = ship;
    }
  }

  hit(ship: Ship) {
    ++ship.hitCount;
    if (ship.hitCount >= ship.offset.length + 1) {
      ship.isSunk = true;
    }
  }

  transform(ship: Ship, offset: Point) {
    switch (ship.orientation) {
      case Orientation.Down: {
        return new Point(ship.origin.x + offset.x, ship.origin.y + offset.y);
      }
      case Orientation.Up: {
        return new Point(ship.origin.x - offset.x, ship.origin.y - offset.y);
      }
      case Orientation.Left: {
        return new Point(ship.origin.x + offset.y, ship.origin.y - offset.x);
      }
      case Orientation.Right: {
        return new Point(ship.origin.x - offset.y, ship.origin.y + offset.x);
      }
    }
  }
}
