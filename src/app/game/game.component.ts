import { Component, OnInit } from '@angular/core';
import {Player} from '../models/player.model';
import {TurnService} from '../services/turn.service';
import {ShipService} from '../services/ship.service';
import {Router} from '@angular/router';
import {PlayerService} from '../services/player.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  players: Player[];
  showTurnOverlay: boolean;
  gameOver: boolean;
  shotsFired: number;
  winner: string;

  constructor(private turnService: TurnService,
              private shipService: ShipService,
              private playerService: PlayerService,
              private router: Router) {
    this.showTurnOverlay = false;
    this.shotsFired = 0;
    this.gameOver = false;
    this.winner = '';
  }

  ngOnInit() {
    //this.showTurnOverlay = false;

    if (sessionStorage.getItem('game')) {
      this.players = JSON.parse(sessionStorage.getItem('game'));

      this.players.forEach((player) => {
        for (let i = 0; i < player.ships.length; i++) {
          this.shipService.applyPosition(player.ships[i], player.board);
        }

      });
    } else {
      this.initGame();
    }
  }

  initGame(oldGame?) {
    let numOfPlayers = 2;
    this.players = [];

    for (let i = 0; i < numOfPlayers; i++) {
      let tmpPlayer = new Player(i);
      this.playerService.generateBoard(tmpPlayer);
      this.players.push(tmpPlayer);
    }



    this.players.forEach((player) => {
      for (let i = 0; i < player.ships.length; i++) {
        if (!oldGame) {
          this.shipService.generatePosition(player.ships[i], player.board);
        }
        this.shipService.applyPosition(player.ships[i], player.board);
      }
    });

    sessionStorage.setItem('game', JSON.stringify(this.players));
    this.turnService.setGame(this.players);
  }

  fire(hitObj) {
    this.shotsFired++;
    sessionStorage.setItem('game', JSON.stringify(this.players));

    if (!hitObj.hit) {
      this.turnService.turnComplete();
      //Check Adjacent
      let adjSpaces = [];
      //Diagonals
      this.arrayPushSanityCheck(adjSpaces, hitObj.space.x + 1, hitObj.space.y + 1);
      this.arrayPushSanityCheck(adjSpaces, hitObj.space.x - 1, hitObj.space.y + 1);
      this.arrayPushSanityCheck(adjSpaces, hitObj.space.x - 1, hitObj.space.y - 1);
      this.arrayPushSanityCheck(adjSpaces, hitObj.space.x + 1, hitObj.space.y - 1);
      //Horizontal and Vertical
      this.arrayPushSanityCheck(adjSpaces, hitObj.space.x, hitObj.space.y + 1);
      this.arrayPushSanityCheck(adjSpaces, hitObj.space.x, hitObj.space.y - 1);
      this.arrayPushSanityCheck(adjSpaces, hitObj.space.x + 1, hitObj.space.y);
      this.arrayPushSanityCheck(adjSpaces, hitObj.space.x - 1, hitObj.space.y);

      adjSpaces.forEach((space) => {
        if (space.ship) {
          hitObj.space.shipAdj = true;
          sessionStorage.setItem('game', JSON.stringify(this.players));
        }
      });

      //Set Turn Message
      document.getElementById('missMessageBox').innerHTML = 'You missed =[' +
        '<br>Player ' + (this.turnService.getTurn() + 1) + '\'s turn.';
      this.toggleOverlay();
      //Clear Game Message
      document.getElementById('hitMessageBox').innerHTML = '';
    } else {
      document.getElementById('hitMessageBox').innerHTML = 'You hit a ship! Take another shot.';
    }


  }

  arrayPushSanityCheck(arr, x, y) {
    let xValid = x >= 0 && x < this.players[this.turnService.getTurn()].board[0].length;
    let yValid = y >= 0 && y < this.players[this.turnService.getTurn()].board.length;

    //x and y within range of board dimensions?
    if (xValid && yValid) {
      arr.push(this.players[this.turnService.getTurn()].board[y][x]);
    } else {
      console.error('You tried to push something that didn\'t exist');
    }
  }

  sunk(sunkObj) {
    let ship = sunkObj.ship;
    document.getElementById('hitMessageBox').innerHTML = 'You Sunk a ' + ship.displayName;
    this.playerService.pushSunkShips(this.players[sunkObj.turnOrder]);

    //All ships sunk?
    if (this.players[sunkObj.turnOrder].ships.length === 0) {
      //Game over
      this.gameOver = true;
      //turnOrder = player that lost
      if (sunkObj.turnOrder) {
        this.winner = 'Player 1';
      } else {
        this.winner = 'Player 2';
      }
    }
  }

  resetGame() {
    sessionStorage.removeItem('game');
    this.turnService.reset();
    this.router.navigateByUrl('/lobby');
  }

  toggleOverlay() {
    this.showTurnOverlay = !this.showTurnOverlay;
  }

}
