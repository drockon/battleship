import {Point} from './point.model';
import {Orientation} from '../orientation.enum';
import {Space} from './space.model';
/**
 * Created by dr483c on 8/5/2017.
 */
export class Ship {
  origin: Point;
  oppositeOrigin: Point;
  offset: Point[];
  orientation: Orientation;
  hitCount: number;
  isSunk: boolean;
  displayName: string;


  constructor() {
    this.origin = new Point(0, 0);
    this.oppositeOrigin = new Point(0, 0);
    this.offset = [];
    this.orientation = 0;
    this.hitCount = 0;
    this.isSunk = false;
    this.displayName = 'Generic Ship';
  }
}
