import {Ship} from './ship.model';
import {Point} from './point.model';
/**
 * Created by dr483c on 8/5/2017.
 */
export class LShip extends Ship {

  constructor() {
    super();
    this.offset = [new Point(0, 1), new Point(0, 2), new Point(1, 2)];
    this.oppositeOrigin = new Point(1, 2);
    this.displayName = 'L Ship';
  }
}

export class BlockShip extends Ship {

  constructor() {
    super();
    this.offset = [new Point(0, 1), new Point(1, 0), new Point(1, 1)];
    this.oppositeOrigin = new Point(1, 1);
    this.displayName = 'Block Ship';
  }
}

export class LineShip extends Ship {

  constructor() {
    super();
    this.offset = [new Point(0, 1), new Point(0, 2), new Point(0, 3)];
    this.oppositeOrigin = new Point(0, 3);
    this.displayName = 'Line Ship';
  }
}

