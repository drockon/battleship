import {Ship} from "./ship.model";
/**
 * Created by dr483c on 8/4/2017.
 */
export class Space {
  x: number;
  y: number;
  ship: Ship;
  hit: boolean;
  turnOrder: number;
  shipAdj: boolean;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
    this.ship = null;
    this.hit = false;
    this.turnOrder = -1;
    this.shipAdj = false;
  }
}
