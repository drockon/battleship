import {Space} from './space.model';
import {Ship} from "./ship.model";
import {BlockShip, LineShip, LShip} from "./other-ship.model";
/**
 * Created by dr483c on 8/4/2017.
 */
export class Player {
  board: Space[][];
  ships: Ship[];
  sunkShips: Ship[]
  turnOrder: number;

  constructor(turnOrder: number) {

    this.ships = [new LShip(), new BlockShip(), new LineShip(), new LineShip()];
    this.sunkShips = [];
    this.board = new Array(8);
    this.turnOrder = turnOrder;
  }


}
