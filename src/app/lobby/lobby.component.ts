import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.css']
})
export class LobbyComponent implements OnInit {
  modalClosed: boolean

  constructor() {
    this.modalClosed = true;
  }

  ngOnInit() {
  }

  toggleRules() {
    this.modalClosed = !this.modalClosed;
  }

}
