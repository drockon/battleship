If you do not already have the Angular CLI run:

npm install -g @angular/cli

After ensuring you have the Angular CLI installed run:

cd {pathToProjectRoot}

ng serve -o
